using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MentalState : MonoBehaviour{

    public static Action onDeath = () => { };
    public static Action<float> onHit = (float x) => { };

    public float maxLevel = 100;
    public float level = 100;

    private void Start() {
        level = maxLevel;
    }

    public void GetHit(float hit) {
        level -= hit;
        onHit.Invoke(GetPercent());
        if (level <= 0) {
            onDeath.Invoke();
        }
    }

    public float GetPercent() {
        return (float)level / maxLevel;
    }

    [ContextMenu("TestGetHit")]
    public void TestGetHit() {
        GetHit(50);
    }


}
