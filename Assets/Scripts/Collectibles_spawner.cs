using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles_spawner : MonoBehaviour
{
    public GameObject Collectible;
    public int amountOfCollectibles;
    public List<Transform> SpawnSpots = new List<Transform>();

    void Start()
    {
        
        for (int i = 0; i < amountOfCollectibles; i++)
        {
            int random = Random.Range(0, SpawnSpots.Count-1);
            GameObject instance = Instantiate(Collectible, SpawnSpots[random].position,Quaternion.identity);
            instance.transform.position = new Vector3(instance.transform.position.x, instance.transform.position.y, 0);
            SpawnSpots.RemoveAt(random);

        }
    }

    
}
