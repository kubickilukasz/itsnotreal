using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour{

    [SerializeField] GameObject toActive;

    bool isActive = false;

    private void OnEnable() {
        Time.timeScale = 1f;
        toActive.SetActive(false);
        MentalState.onDeath += OnDeath;
    }

    private void OnDisable() {
        MentalState.onDeath -= OnDeath;
    }

    private void Update() {
        if (isActive && Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene(0);
        }
    }

    void OnDeath() {
        isActive = true;
        Time.timeScale = 0f;
        toActive.SetActive(true);
    }

}
