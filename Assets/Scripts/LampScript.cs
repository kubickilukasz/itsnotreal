using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampScript : MonoBehaviour
{
    public SpriteRenderer SpriteRenderer;
    public Sprite Lamp_lit;
    public Sprite Lamp_not_lit;
    public GameObject LightHalo;
    public ParticleSystem flames;
    public ParticleSystem flame_burst;
    public bool isLit = true;

    public GameObject obstacleForEnemy;

    [SerializeField]
    [Range(0, 360)]
    public float ViewAngle = 50;
    [SerializeField]
    [Range(0.1f, 100)]
    public float spriteMeshResolution = 15;
    public float LightRange = 10;
    public int edgeResolveIterations = 10;
    public LayerMask ObstacleMask;
    [SerializeField]
    LayerMask EnemiesMask;
    [SerializeField]
    public float RangeOfView = 6;

    public MeshRenderer Renderer;
    public MeshFilter viewMeshFilter;
    public Mesh viewMesh;

    public List<Enemy> EnemiesInLight = new List<Enemy>();

    private void Start()
    {
        viewMesh = new Mesh();
        viewMesh.name = "Mesh (bez nazwy jakies2)";
        viewMeshFilter.mesh = viewMesh;
        Renderer.sortingOrder = 10;
        LightHalo.SetActive(true);
        DrawLightSprite();


    }
    public void DisableLamp()
    {
        SpriteRenderer.sprite = Lamp_not_lit;
        flames.Stop();
        flame_burst.Play();
        LightHalo.SetActive(false);
        viewMesh.Clear();
        isLit = false;
        Debug.Log("Disabling a lamp");
        obstacleForEnemy.SetActive(false);
        GenerateGrid.Instance.Generate();
    }
    private void Update()
    {
        
        FindVisibleTargets();
    }

    public void FindVisibleTargets() {
        Vector2 CharacterPosition = Pos3ToPos2(transform.position);     //new Vector2(PlayerTransform.position.x, PlayerTransform.position.y);
        Collider2D[] EnemiesInRange = Physics2D.OverlapCircleAll(CharacterPosition, RangeOfView, EnemiesMask);

        EnemiesInLight.ForEach(x => x.IsInNotLight());
        EnemiesInLight.Clear();
        for (int i = 0; i < EnemiesInRange.Length; i++) {
            Transform enemy = EnemiesInRange[i].transform;
            Vector2 directionToEnemy = Pos3ToPos2(enemy.position) - Pos3ToPos2(transform.position);
            if (Vector2.Angle(Pos3ToPos2(transform.up), directionToEnemy) < ViewAngle / 2) {
                float distanceToEnemy = Vector2.Distance(Pos3ToPos2(enemy.position), Pos3ToPos2(transform.position));
                if (!Physics2D.Raycast(Pos3ToPos2(transform.position), directionToEnemy, distanceToEnemy, ObstacleMask)) {
                    Enemy tempEnemy = enemy.GetComponent<Enemy>();
                    if (tempEnemy != null) {
                        tempEnemy.IsInLight(EnemiesInRange[i].transform.position);
                        EnemiesInLight.Add(tempEnemy);
                    }

                }
            }
        }
    }


    Vector2 Pos3ToPos2(Vector3 pos3) {
        return new Vector2(pos3.x, pos3.y);
    }

    public void DrawLightSprite() {
        int stepCount = Mathf.RoundToInt(spriteMeshResolution * ViewAngle);
        float stepAngleSize = ViewAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();

        for (int i = 0; i < stepCount; i++) {
            float angle = transform.eulerAngles.z - ViewAngle / 2 + stepAngleSize * i;

            Vector2 vecForConversionFromVec2ToVec3 = DirectionFromAngle(angle, true);
            Vector3 direction = new Vector3(-vecForConversionFromVec2ToVec3.x, vecForConversionFromVec2ToVec3.y, 0); // minus x because of the conversion from 2d to 3d spaces
            ViewCastInfo newViewCast = ViewCast(angle);
            if (i > 0) {
                if (oldViewCast.hit != newViewCast.hit) {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.PointA != Vector3.zero) {
                        viewPoints.Add(edge.PointA);
                    }
                    if (edge.PointB != Vector3.zero) {
                        viewPoints.Add(edge.PointB);
                    }
                }

            }
            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
            //Debug.DrawLine(transform.position, transform.position + direction * RangeOfView, Color.white);
        }
        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertecies = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];
        vertecies[0] = Vector3.zero;

        for (int i = 0; i < vertexCount - 1; i++) {
            vertecies[i + 1] = transform.InverseTransformPoint(viewPoints[i]);
        }
        for (int i = vertexCount; i > 0; i--) {
            if (i < vertexCount - 2) {
                triangles[i * 3] = i + 2;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = 0;
            }
        }
        viewMesh.Clear();
        viewMesh.vertices = vertecies;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }
    ViewCastInfo ViewCast(float globalAngle) {
        Vector2 vecForConversionFromVec2ToVec3 = DirectionFromAngle(globalAngle, true);
        Vector3 direction = new Vector3(-vecForConversionFromVec2ToVec3.x, vecForConversionFromVec2ToVec3.y, 0);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, LightRange, ObstacleMask);         //Physics2D.GetRayIntersection(new Ray(transform.position, direction), RangeOfView, ObstacleMask); // Physics2D.Raycast(transform.position, direction, RangeOfView, ObstacleMask);
        if (hit.collider != null) {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        return new ViewCastInfo(false, transform.position + direction * LightRange, LightRange, globalAngle);
    }

    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast) {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;
        for (int i = 0; i < edgeResolveIterations; i++) {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);
            if (newViewCast.hit == minViewCast.hit) {
                minAngle = angle;
                minPoint = newViewCast.point;
            } else {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }
        return new EdgeInfo(minPoint, maxPoint);
    }

    public Vector2 DirectionFromAngle(float angle, bool angleIsGlobal) {

        if (!angleIsGlobal) {
            angle -= transform.eulerAngles.z;
        }
        return new Vector2(Mathf.Sin(angle * Mathf.Deg2Rad), Mathf.Cos(angle * Mathf.Deg2Rad));
    }



    public struct ViewCastInfo
    {
        public bool hit;
        public Vector2 point;
        public float distance;
        public float angle;
        public ViewCastInfo(bool _hit, Vector2 _point, float _distance, float _angle)
        {
            hit = _hit;
            point = _point;
            distance = _distance;
            angle = _angle;
        }

    }
    public struct EdgeInfo
    {
        public Vector3 PointA;
        public Vector3 PointB;

        public EdgeInfo(Vector3 _PointA, Vector3 _PointB)
        {
            PointA = _PointA;
            PointB = _PointB;
        }
    }
}
