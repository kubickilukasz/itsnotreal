using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;

[ExecuteInEditMode]
public class GenerateGrid : Singleton<GenerateGrid> {

    [SerializeField] float cellSize = 1f;
    [SerializeField] float cellSizeBound = 1f; // TODO 
    [SerializeField] Transform topLeftCorner, bottomRightCorner;
    [SerializeField] LayerMask blockedMaskObjects;

    [SerializeField, HideInInspector]
    List<GridCell> gridCells;
    GridCell[,] gridCellsArray;
    [SerializeField, HideInInspector]
    int sizeX, sizeY;

    void Awake() {
        Generate();
        gridCellsArray = new GridCell[sizeX, sizeY];
        foreach (GridCell cell in gridCells) {
            gridCellsArray[cell.x, cell.y] = cell;
        }

    }

 
    public void Generate() {
        sizeX = (int)(Mathf.Abs(topLeftCorner.position.x - bottomRightCorner.position.x) / cellSizeBound + 1);
        sizeY = (int)(Mathf.Abs(topLeftCorner.position.y - bottomRightCorner.position.y) / cellSizeBound + 1);
        gridCells = new List<GridCell>(sizeX * sizeY);
        Vector2 sizeCell = new Vector2(cellSize, cellSize);
        Vector2 position = new Vector2(topLeftCorner.position.x, topLeftCorner.position.y);
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                GridCell newGridCell = new GridCell(position, x, y, sizeCell, blockedMaskObjects);
                position -= new Vector2(0, cellSizeBound);
                gridCells.Add(newGridCell);
            }
            position += new Vector2(cellSizeBound, 0);
            position.y = topLeftCorner.position.y;
        }
    }

    public GridCell GetCellByPosition(Vector2 position) {
        int x = (int)(Mathf.Abs(topLeftCorner.position.x - position.x) / cellSizeBound);
        int y = (int)(Mathf.Abs(topLeftCorner.position.y - position.y) / cellSizeBound);
        x = Mathf.Min(x, sizeX - 1);
        y = Mathf.Min(y, sizeY - 1);

        x = Mathf.Max(x, 0);
        y = Mathf.Max(y, 0);

        return gridCellsArray[x, y];
    }

    public List<GridCell> GetNotBlockedNeighbours(int x, int y) {
        List<GridCell> cells = new List<GridCell>(4);
        AddGridCellToList(x + 1, y, cells);
        AddGridCellToList(x - 1, y, cells);
        AddGridCellToList(x, y + 1, cells);
        AddGridCellToList(x, y - 1, cells);
        return cells;
    } 

    void AddGridCellToList(int x, int y, List<GridCell> list) {
        if (x < 0 || x >= sizeX || y < 0 || y >= sizeY) {
            return;
        }
        list.Add(gridCellsArray[x,y]);
    }

    void OnDrawGizmosSelected() {
        foreach(GridCell cell in gridCells){
            Gizmos.color = cell.isBlocked ? Color.black : Color.red;
            Gizmos.DrawWireCube(cell.position, new Vector3(cellSize * 0.9f, cellSize * 0.9f, cellSize * 0.9f));
        }
        if (topLeftCorner != null) {
            Gizmos.color = Color.green; ;
            Gizmos.DrawWireCube(topLeftCorner.position, new Vector3(0.1f, 0.1f, 0.1f));
        }
        if (bottomRightCorner != null) {
            Gizmos.color = Color.green; ;
            Gizmos.DrawWireCube(bottomRightCorner.position, new Vector3(0.1f, 0.1f, 0.1f));
        }
    }
}

[System.Serializable]
public class GridCell {
    public Vector2 position;
    public bool isBlocked;
    public int x, y;

    [System.NonSerialized] public List<GridCell> neighbours = null;
    public int Cost { get; set; }
    public float Distance { get; set; }
    public float CostDistance => Cost + Distance;
    public GridCell parent { get; set; } = null;

    private GridCell() { }

    public GridCell(Vector2 position, int x, int y, Vector2 size, LayerMask blockedObjectMask) {
        this.position = position;
        this.x = x;
        this.y = y;
        isBlocked = Physics2D.OverlapBox(position, size, 0, blockedObjectMask) != null;
    }
}

