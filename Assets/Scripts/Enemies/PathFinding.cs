using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PathFinding : Singleton<PathFinding>{

    public Queue<GridCell> GetPath(Vector2 startPosition, Vector2 target) {
        GridCell startGridCell = GenerateGrid.Instance.GetCellByPosition(startPosition);
        GridCell endGridCell = GenerateGrid.Instance.GetCellByPosition(target);

        if (startGridCell == null || endGridCell == null || endGridCell.isBlocked == true || startGridCell.Equals(endGridCell)) {
            return null;
        }

        List<GridCell> currentPath = new List<GridCell>();
        List<GridCell> usedGridCells = new List<GridCell>();
        usedGridCells.Add(startGridCell);

        if (SearchInNeighbours(startGridCell, endGridCell, currentPath, usedGridCells)) {
            Queue<GridCell> queque = new Queue<GridCell>();
            foreach (GridCell gridCell in currentPath) {
                queque.Enqueue(gridCell);
            }
            return queque;
        } else {
            return null;
        }
    }

    bool SearchInNeighbours(GridCell currentCell, GridCell target, List<GridCell> path, List<GridCell> usedGridCells) {
        if (currentCell.Equals(target)) {
            return true;
        }

        if (currentCell.neighbours == null) {
            currentCell.neighbours = GenerateGrid.Instance.GetNotBlockedNeighbours(currentCell.x, currentCell.y);
        }

        List<GridCell> neig = new List<GridCell>();
        foreach (GridCell g in currentCell.neighbours) {
            if (usedGridCells.Contains(g) || g.isBlocked) {
                continue;
            }
            g.parent = currentCell;
            g.Cost = 1;
            g.Distance = Vector2.Distance(g.position, target.position);
            neig.Add(g);
        }

        neig.Sort((x, y) => x.CostDistance >= y.CostDistance ? 1 : -1);

        foreach (GridCell g in neig) {
            usedGridCells.Add(g);
            path.Add(g);
            if (SearchInNeighbours(g, target, path, usedGridCells)) {
                return true;
            } else {
                path.RemoveAt(path.Count - 1);
            }
        }

        return false;

    }


}

