using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor (typeof(LightBeam))]
public class LightBeamEditor : Editor
{
     void OnSceneGUI()
    {
        LightBeam lb = (LightBeam)target;
        Handles.color = Color.yellow;
        Handles.DrawWireArc(lb.transform.position, Vector3.forward, Vector3.right, 360, lb.RangeOfView);
        Vector3 viewAngle1 = lb.DirectionFromAngle(-lb.ViewAngle / 2, false);
        Vector3 viewAngle2 = lb.DirectionFromAngle(lb.ViewAngle / 2, false);
        Handles.DrawLine(lb.transform.position, lb.transform.position + viewAngle1 * lb.RangeOfView);
        Handles.DrawLine(lb.transform.position, lb.transform.position + viewAngle2 * lb.RangeOfView);


        Handles.color = Color.red;
        foreach(Enemy enemy in lb.EnemiesInLight)
        {
            Handles.DrawLine(lb.transform.position, enemy.transform.position);
        }
    }
}
