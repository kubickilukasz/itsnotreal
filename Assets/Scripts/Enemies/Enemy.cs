using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour{

    [SerializeField] protected float speed = 2.5f;
    [SerializeField] protected float distanceToCellGridPath = 0.25f;
    [SerializeField] protected float distanceChangeTargetToRefreshPath = 1f;
    [SerializeField] protected float hitPoints = 10f;
    [SerializeField] protected AnimationCurve speedByDistance;

    protected Queue<GridCell> path = new Queue<GridCell>();

    Vector3 cacheCurrentPosition;
    Transform currentTarget = null;
    GridCell currentGridCell = null;

    protected Vector3 CurrentTarget => currentTarget.position;
    protected Vector3 CurrentGridCell => currentGridCell != null ? new Vector3(currentGridCell.position.x, currentGridCell.position.y) : transform.position;

    Transform player;
    Transform idleTarget;

    float timer = 0;

    bool isVisible = false;
    protected bool IsVisible => isVisible;

    public virtual void Init() {
        currentTarget = idleTarget = (new GameObject("Idlespot " + gameObject.name)).transform;
        idleTarget.position = transform.position;
    }

    public void SetPlayer(Transform player) {
        this.player = player;
    }

    public void IsInLight(Vector3 positionCollision) {
        isVisible = true;
        StopMove();
    }

    public void IsInNotLight() {
        isVisible = false;
        currentTarget = idleTarget;
        cacheCurrentPosition = idleTarget.position;
    }

    public void ChangeOnIdle() {
        timer = 0f;
        currentTarget = idleTarget;
        cacheCurrentPosition = idleTarget.position;
        SetPath();
    }

    public void ChangeOnPlayer() {
        timer = 0f;
        currentTarget = player;
        cacheCurrentPosition = player.position;
        SetPath();
    }

    public void StopMove() {
        if (currentTarget != null) {
            timer = 0f;
            cacheCurrentPosition = currentTarget.position;
            currentTarget = null;
            path = null;
        }
    }

    public void DestroyMe() {
        Destroy(gameObject);
        EnemyManager.Instance.Refresh();
    }

    void Update() {
        OnUpdate();
        RefreshPath();
    }

    void OnDestroy() {
        if (idleTarget) {
            Destroy(idleTarget.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (!isVisible) {
            MentalState mentalState = collision.gameObject.GetComponent<MentalState>();
            if (mentalState) {
                mentalState.GetHit(hitPoints);
                DestroyMe();
            }
        }
    }

    protected abstract void OnUpdate();

    protected void RefreshPath() {
        if (currentTarget != null && (Vector3.Distance(cacheCurrentPosition, currentTarget.position) > distanceChangeTargetToRefreshPath || path == null)) {
            cacheCurrentPosition = currentTarget.position;
            SetPath();
            print(path);
        }
    }

    protected float DistanceFromPlayer() {
        if (player != null) {
            return Vector3.Distance(player.position, transform.position);
        }
        return float.MaxValue;
    }

    protected void MoveFromPath() {
        if (currentGridCell != null) {
            timer += Time.deltaTime;
            float tempSpeed = player == currentTarget ? speedByDistance.Evaluate(DistanceFromPlayer()) : speed;
            transform.Translate((currentGridCell.position - new Vector2(transform.position.x, transform.position.y)).normalized * tempSpeed * Time.deltaTime);
            if (Vector3.Distance(currentGridCell.position, transform.position) <= distanceToCellGridPath) {
                if (path != null && path.Count > 0) {
                    currentGridCell = path.Dequeue();
                } else {
                    currentGridCell = null;
                }
            }
        } else if (path != null && path.Count > 0) {
            currentGridCell = path.Dequeue();
        }
    }


    void SetPath() {
        Queue<GridCell> cells = PathFinding.Instance.GetPath(transform.position, currentTarget.position);
        if (cells != null) {
            path = cells;
        }
    }

}

