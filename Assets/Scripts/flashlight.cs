using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashlight : MonoBehaviour
{
    public float battery =100;
    public float batteryUsageMultiplier = 1;
    public LightBeam lb;


    // Update is called once per frame
    void Update()
    {

       if(battery>0) battery -= 1 * Time.deltaTime; 
       else if (battery <= 0){
            lb.enabled = false;
            lb.viewMesh.Clear();

        }
    }
     void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Lamp"))
        {

            LampScript ls = collision.GetComponent<LampScript>();
            if (!ls.isLit) return;
            ls.DisableLamp();
            battery = 200;
            if (!lb.enabled) lb.enabled = true;
        }
    }
}
