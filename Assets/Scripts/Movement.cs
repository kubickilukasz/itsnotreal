﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Privates ( ͡° ͜ʖ ͡°)
    [SerializeField]
    Rigidbody2D Rb;
    [SerializeField]
    Vector2 displace;
    Camera mainCam;
    // Publics
    public float MovementSpeed = 5f;
    public float InterpolationSmoothness = 5f;
    public Texture2D CursorTexture;
    // Start is called before the first frame update
    void Start(){
        mainCam = Camera.main;
        Cursor.visible = false;
        Cursor.SetCursor(CursorTexture, Vector2.zero, CursorMode.ForceSoftware);
    }

    // Update is called once per frame
    void Update(){
        LookAtMouse();
        displace.x = Input.GetAxisRaw("Horizontal");
        displace.y = Input.GetAxisRaw("Vertical");


        displace.Normalize();
    }
    private void FixedUpdate(){
        Vector3 movement = new Vector3(displace.x, displace.y, 0);
        movement *= MovementSpeed * Time.deltaTime;
        transform.SetPositionAndRotation(transform.position + movement, transform.rotation);
         //Rb.MovePosition(Rb.position + displace * MovementSpeed * Time.deltaTime);
       // Rb.AddRelativeForce();
    }

    void LookAtMouse()
    {
        
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, Mathf.LerpAngle(transform.rotation.eulerAngles.z, rot_z - 90 , InterpolationSmoothness)); 

    }
}