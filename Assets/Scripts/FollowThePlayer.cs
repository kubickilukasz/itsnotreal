using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowThePlayer : MonoBehaviour
{
    [SerializeField]
    Transform Player;
    void LateUpdate()
    {
        Vector3 new_cam_pos = new Vector3(Player.position.x, Player.position.y, transform.position.z);
        transform.SetPositionAndRotation(new_cam_pos, transform.rotation);
    }
}
