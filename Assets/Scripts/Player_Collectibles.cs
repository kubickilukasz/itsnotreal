using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player_Collectibles : MonoBehaviour
{
    public Collectibles_spawner SpawnerScript;
    public int AmountCollected=0;
    public int AmountOfCollectibles;
    public static Action onFound = () => { };
    private void Start()
    {
        AmountOfCollectibles = SpawnerScript.amountOfCollectibles;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Znajdzka znalezioned !");
       
        if (collision.CompareTag("Collectible"))
        {

           AmountCollected += 1;
            if(AmountCollected <= AmountOfCollectibles)
            {
                // WINEK HERE
                onFound.Invoke();

            }
            Destroy(collision.gameObject);
        }
    }
}
