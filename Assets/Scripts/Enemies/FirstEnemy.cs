using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstEnemy : Enemy {
    [SerializeField] float distanceToPlayerToChangeState = 10f;

    bool isIdle = true;

    public override void Init() {
        base.Init();
    }

    protected override void OnUpdate() {
        if (IsVisible) {
            isIdle = true;
            return;
        }

        if (distanceToPlayerToChangeState < DistanceFromPlayer()) {
            if (!isIdle) {
                OnStartIdle();
                isIdle = true;
            }
            OnUpdateIdle();
        } else {
            if (isIdle) {
                OnStartPlayer();
                isIdle = false;
            }
            OnUpdatePlayer();
        }
    }

    void OnStartIdle() {
        ChangeOnIdle();
    }

    void OnUpdateIdle() {
        MoveFromPath();
    }

    void OnStartPlayer() {
        ChangeOnPlayer();
    }

    void OnUpdatePlayer() {
        MoveFromPath();
    }

    private void OnDrawGizmosSelected() {
        //return;
        Color color = new Color(1, 0, 0, 1);
        if (path != null) {
            foreach (GridCell cell in path) {
                Gizmos.color = color;
                Gizmos.DrawWireCube(cell.position, new Vector3(0.3f,  0.3f,  0.3f));
                color.b += 0.1f;
            }
        }

        Gizmos.DrawCube(CurrentGridCell, new Vector3(0.3f, 0.3f, 0.3f));
    }

}
