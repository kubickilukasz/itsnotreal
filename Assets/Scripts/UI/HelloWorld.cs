using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HelloWorld : MonoBehaviour{

    [SerializeField] List<GameObject> onStart = new List<GameObject>();
    [SerializeField] List<GameObject> onEnd = new List<GameObject>();
    [SerializeField] List<GameObject> endGame = new List<GameObject>();

    void Start() {
        onStart.ForEach(x => x.SetActive(true));
        onEnd.ForEach(x => x.SetActive(false));
        endGame.ForEach(x => x.SetActive(false));
    }

    public void NewGame() {
        onStart.ForEach(x => x.SetActive(false));
        endGame.ForEach(x => x.SetActive(false));
        onEnd.ForEach(x => x.SetActive(true));
    }

    public void OnEndGame() {
        endGame.ForEach(x => x.SetActive(true));
        onStart.ForEach(x => x.SetActive(false));
        onEnd.ForEach(x => x.SetActive(false));
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            NewGame();
        }
    }

}
