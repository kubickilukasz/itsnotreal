using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : Singleton<EnemyManager>{

    [SerializeField] Transform player;

    List<Enemy> enemies;

    void Start(){
        enemies = new List<Enemy>(GetComponentsInChildren<Enemy>());
        foreach (Enemy enemy in enemies) {
            enemy.SetPlayer(player);
            enemy.Init();
        }
    }

    public void Create<T>() where T : Enemy{
        //TODO
    }

    public void Refresh() {
        enemies = new List<Enemy>(GetComponentsInChildren<Enemy>());
    }

}
