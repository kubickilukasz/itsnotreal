using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Battery : MonoBehaviour{

    [SerializeField]
    flashlight _flashlight;

    [SerializeField]
    List<GameObject> images;

    float maxBattery;

    private void Start() {
        maxBattery = _flashlight.battery;
    }

    private void Update() {
        int bound = Mathf.Min((int)(images.Count * (_flashlight.battery / maxBattery) + 1), images.Count);
        for (int i = 0; i < bound; i++) {
            images[i].SetActive(true);
        }
        for (int i = bound; i < images.Count; i++) {
            images[i].SetActive(false);
        }

    }

}
