using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brain : MonoBehaviour{

    [SerializeField] Image image;
    [SerializeField] List<Sprite> sprites = new List<Sprite>();

    private void OnEnable() {

        MentalState.onHit += OnHit;
    }

    private void OnDisable() {
        MentalState.onHit -= OnHit;
    }

    private void OnHit(float percent) {
        int i = (int)((sprites.Count) * percent);
        i = Mathf.Min(i, sprites.Count - 1);
        image.sprite = sprites[i];
    }

}
